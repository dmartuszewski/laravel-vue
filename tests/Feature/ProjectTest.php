<?php

namespace Tests\Feature;

use App\Models\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    public function test_projects_list_available()
    {
        $response = $this->get('/api/projects');

        $response->assertStatus(200);
    }

    public function test_creating_project()
    {
        $projectData = [
            'title' => 'Test project',
            'description' => 'Project description',
        ];

        $response = $this->json('POST', '/api/projects', $projectData);

        $response
            ->assertStatus(201)
            ->assertJson(['project' => $projectData]);
    }

    public function test_creating_project_without_title()
    {
        $projectData = [
            'description' => 'Project description',
        ];

        $response = $this->json('POST', '/api/projects', $projectData);

        $response
            ->assertStatus(422)
            ->assertJson(['title' => ['The title field is required.']]);
    }

    public function test_updating_project()
    {
        $project = Project::factory()->create();

        $projectData = [
            'title' => 'This is updated title'
        ];

        $response = $this->json('PATCH', '/api/projects/' . $project->id, $projectData);

        $response
            ->assertStatus(200)
            ->assertJson(['project' => $projectData]);
    }
}
