<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProjectResource::collection(Project::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return JsonResponse
     */
    public function store(StoreProjectRequest $request): JsonResponse
    {
        $project = new Project($request->validated());

        if ($project->save()) {
            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'Project created',
                    'project' => new ProjectResource($project)
                ],
                201
            );
        }

        return response()->json(
            [
                'message' => 'Project creating failed'
            ],
            500
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     *
     * @return ProjectResource
     */
    public function show(Project $project): ProjectResource
    {
        $project->load('tasks');

        return new ProjectResource($project);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param StoreProjectRequest $request
     * @param Project $project
     *
     * @return JsonResponse
     */
    public function update(StoreProjectRequest $request, Project $project): JsonResponse
    {
        $project->fill($request->validated());

        if ($project->save()) {
            return response()->json(
                [
                    'message' => 'Project updated',
                    'project' => new ProjectResource($project)
                ]
            );
        }

        return response()->json(
            [
                'message' => 'Project updating failed'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     *
     * @return JsonResponse
     */
    public function destroy(Project $project): JsonResponse
    {
        try {
            $project->delete();
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => 'Project cannot be deleted'
                ],
                202
            );
        }

        return response()->json(
            [
                'message' => 'Project removed'
            ],
            200
        );
    }
}
