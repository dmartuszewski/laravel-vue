# Laravel Vue: Projects and tasks 

## Setup (using Homestead)
1. Create virtual host (via Homestead.yaml) laravel-vue.test and add database laravel-vue
2. Add `laravel-vue.test` to your `/etc/hosts` pointing at Homestead virtual machine
3. Copy `.env.example` to `.env`
4. Install PHP dependencies by running `composer install` 
4. Set correct database credentials in your .env file (if different from default)
5. Open https://laravel-vue.test/ in your browser

## Api docs (Postman)
https://documenter.getpostman.com/view/1182124/Tz5iC21E

## Todo
- Authentication & authorization (Policies)
- Projects pagination
- Proper tests suite (more & detailed tests)
- Cleaner UI
- Error handling on frontend
- Vue.js tests
- Marking tasks of done/undone
- General code cleanup
