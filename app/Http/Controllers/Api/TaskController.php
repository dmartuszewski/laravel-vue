<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return JsonResponse
     */
    public function store(StoreTaskRequest $request): JsonResponse
    {
        $task = new Task($request->validated());
        $task->project_id = $request->project_id;

        if ($task->save()) {
            return response()->json(
                [
                    'message' => 'Task created',
                    'task' => new TaskResource($task),
                ],
                201
            );
        }

        return response()->json(
            [
                'message' => 'Task creating failed'
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Task $task
     *
     * @return JsonResponse
     */
    public function update(Request $request, Task $task): JsonResponse
    {
        $task->fill($request->validated());

        if ($task->save()) {
            return response()->json(
                [
                    'message' => 'Task created',
                    'task' => new TaskResource($task),
                ],
                202
            );
        }

        return response()->json(
            [
                'message' => 'Task updating failed'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     *
     * @return JsonResponse
     */
    public function destroy(Task $task): JsonResponse
    {
        try {
            $task->delete();
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => 'Task cannot be deleted'
                ],
                202
            );
        }

        return response()->json(
            [
                'message' => 'Task removed'
            ],
            200
        );
    }
}
