import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.config.productionTip = false

Vue.use(VueRouter)

import Dashboard from './components/Dashboard.vue';
import AdminPanel from './components/AdminPanel.vue';
import ClientPanel from './components/ClientPanel.vue';

const routes = [
    { path: '/admin', component: AdminPanel },
    { path: '/client', component: ClientPanel },
]

const router = new VueRouter({
    mode: 'history',
    routes
})

new Vue({
    router,
    render: h => h(Dashboard)
}).$mount('#app')
