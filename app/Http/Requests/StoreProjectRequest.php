<?php

namespace App\Http\Requests;


class StoreProjectRequest extends ApiFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:255',
            'description' => 'nullable|max:10000',
        ];
    }
}
