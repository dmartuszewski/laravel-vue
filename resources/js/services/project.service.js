import axios from 'axios'
import {API_URL} from "../config"

class ProjectService {
    getAll() {
        return axios.get(API_URL + '/projects')
    }

    get(id) {
        return axios.get(API_URL + '/projects/' + id)
    }

    save(data) {
        return axios.post(API_URL + '/projects', data)
    }

    update(id, data) {
        return axios.put(API_URL + '/projects/' + id, data)
    }

    delete(id) {
        return axios.delete(API_URL + '/projects/' + id)
    }
}

export default new ProjectService()
