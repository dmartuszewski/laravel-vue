import axios from 'axios'
import {API_URL} from "../config"

class TaskService {
    save(data) {
        return axios.post(API_URL + '/tasks', data)
    }

    markAsDone(id) {
        data = {done: 1}
        return axios.put(API_URL + '/tasks/' + id, data)
    }

    markAsUnDone(id) {
        data = {done: 0}
        return axios.put(API_URL + '/tasks/' + id, data)
    }

    delete(id) {
        return axios.delete(API_URL + '/tasks/' + id)
    }
}

export default new TaskService()
